---
layout: job_page
title: "Liaison"
---

Liaison means you are the communicator, the mediator, the go-between to talk to, regarding your field of expertise within a certain topic.
There can only be one liaison per field of expertise per topic.

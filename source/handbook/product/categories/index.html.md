---
layout: markdown_page
title: Product categories
---

## Introduction

Below is the canonical list of product categories, grouped by DevOps lifecycle
stage and non-lifecycle groups, along with the name of the responsible product manager.
Some of the product categories have an additional level of organization for feature categories.

We want intuitive interfaces both within the company and with the wider community.
This makes it more efficient for everyone to contribute or to get a question answered.
Therefore, the following interfaces are based on the product categories defined on this page:

- [Product Vision](/direction/product-vision/)
- [Direction](/direction/#functional-areas)
- [Software Development Life-Cycle (SDLC)](/sdlc/#stacks)
- [Product Features](/features/)
- [Documentation](https://docs.gitlab.com/ee/#complete-devops-with-gitlab)
- [Backend teams](/handbook/backend/)
- [Product manager responsibilities](/handbook/product/#who-to-talk-to-for-what)
- Our deck, the slides that we use to describe the company
- Product marketing specializations

![DevOps lifecycle](handbook/sales/devops-loop.svg)

At GitLab the Dev and Ops split is different because our CI/CD functionality is one codebase that falls under Ops.

## Dev

- Product: [Job]
- Backend: [Tommy]
- Product Marketing: [John]

1. Plan - [Victor]
    - Project management
        - Issue tracking (assignees, milestones, time tracking, due dates, labels, weights, quick actions, email notifications, todos, search, Elasticsearch integration, Jira and other third-party issue management integration)
        - Issue boards
    - Portfolio management
        - Epics
        - Roadmaps
    - [Service desk]
    - Chat integration (Mattermost, Slack)
1. Create - [Victor] and [James]
    - Source code management
        - Version control / Git repository management (Commits, file locking, LFS, protected branches, project templates, import/export, mirroring, housekeeping (e.g. git gc), hooks) - [James]
        - Code review (merge requests, diffs, approvals) - [Victor]
        - Gitaly - [James]
        - [Geo] - [James]
        - Snippets - [Victor]
        - Markdown - [Victor]
    - Web IDE - [James]
    - Wiki - [James]
1. Auth - [Jeremy], [James], and [Victor]
    - User management & authentication (incl. LDAP, signup, abuse) - [Jeremy]
    - Groups and [Subgroups] - [Jeremy]
    - Navigation - [Jeremy]
    - Audit log - [Jeremy]
    - GitLab.com (our hosted offering of GitLab) - [Jeremy]
    - Subscriptions (incl. license.gitlab.com and customers.gitlab.com) - [Jeremy]
    - [Internationalization](https://docs.gitlab.com/ee/development/i18n/) - [Jeremy]
    - Reporting & Analytics - [James] and [Victor]
        - Cycle Analytics - [James]
        - DevOps Score (previously Conversational Development Index / ConvDev Index) - [James]
        - [Usage statistics](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html) (Version check (incl. version.gitlab.com), Usage ping) - [Victor]
1. Gitter - n/a
1. Distribution - [Josh]
    - Omnibus
    - Cloud Native Installation

## Ops

- Product: [Mark]
- Backend: [Dalia]
- Product Marketing: [William]

1. Verify - [Fabio]
    - [Continuous Integration (CI)]
        - GitLab Runner
    - Security Testing
        - Static Application Security Testing (SAST)
        - Dynamic application security testing (DAST)
        - Dependency Scanning
        - Container Scanning
        - License Management
        - Runtime Application Self-Protection (RASP)
1. Package - [Josh]
    - Container Registry
    - Binary Repository
1. Release - [Fabio]
    - [Continuous Delivery (CD)] / Release Automation
        - Review apps
    - [Pages]
1. Configure - [Daniel]
    - Application Control Panel
        - Auto DevOps
    - Infrastructure Configuration
    - Operations
        - ChatOps
    - Feature Management
        - Feature flags
1. Monitor - [Josh]
    - Application Performance Monitoring (APM)
        - Metrics
        - Tracing
    - Production monitoring
    - Error Tracking
    - Logging
1. BizOps - [Josh]

## Composed categories

GitLab also does the things below that are composed of multiple categories.

1. Software Composition Analysis (SCA) = Dependency Scanning + License Management
1. Interactive Application Security Testing (IAST) = Dynamic application security testing (DAST) + Runtime Application Self-Protection (RASP)

[Jeremy]: /team/#d3arWatson
[Fabio]: /team/#bikebilly
[Josh]: /team/#joshlambert
[Mark]: /team/#MarkPundsack
[William]: /team/#thewilliamchia
[James]: /team/#jamesramsay
[Job]: /team/#Jobvo
[John]: /team/#j_jeremiah
[Victor]: /team/#victorwu416
[Daniel]: /team/#danielgruesso
[Tommy]: /team/#tommy.morgan
[Dalia]: /team/#dhavens
[Pages]: /features/pages/
[Geo]: /features/gitlab-geo/
[Continuous Integration (CI)]: /features/gitlab-ci-cd/
[Continuous Delivery (CD)]: /features/gitlab-ci-cd/
[Subgroups]: /features/subgroups/
[Service Desk]: /features/service-desk/

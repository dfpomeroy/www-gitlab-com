---
layout: markdown_page
title: "Corporate Marketing"
---

## Welcome to the Coporate Marketing Handbook
{:.no_toc}

The Corporate Marketing team includes Global Programs, Customer Marketing, Corporate Events and Design. Corporate Marketing is responsible for the stewardship of the GitLab brand the company messaging/positioning. The team is the owner of the Marketing website and oversees the website strategy. Corporate Marketing creates integrated marketing campaigns, executing global corporate campaigns and supporting the field marketing teams in localizing and verticalizing campaigns for in region execution. The team also oversees Public Relations (PR), the customer reference program and customer marketing.
{: .note}
----

## On this page
{:.no_toc}

* Will be replaced with the ToC, excluding the "On this page" header
{:toc}

----

## Design

### Requesting design help

1. Create an issue in the corresponding project repository
    1. For tasks pertaining to [about.gitlab.com](/) create an issue in the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com/issues).
    1. For all other marketing related tasks create an issue in the [marketing project](https://gitlab.com/gitlab-com/marketing/general/issues).
1. Add all relevant details, goal(s), purpose, resources, and links in the issue description. Also `@` mention team members who will be involved.
1. Set due date (if possible) — small last-minute requests are OK, we understand, but in order to do our best work let's not make a habit of it.
1. Add the `Design` and `Website Redesign` (if applicable) label(s) to your issue.

### The `Design` label in issue tracker

The `Design` label helps us find and track issues relevant to the Design team. If you create an issue where Design is the primary focus, please use this label.

### Project prioritization

Per the Design team's discretion, the prioritization of design projects will be based on the direct impact on Marketing.

To get a better sense of [marketing project](https://gitlab.com/gitlab-com/marketing/general/issues) prioritization, you can view the [Design Issue Board](https://gitlab.com/gitlab-com/marketing/general/boards/273272). More specifically the `Design - Working On` and `Design - Top Priority` lists.

Design projects within the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com/issues) can be tracked using the [Design](https://gitlab.com/gitlab-com/www-gitlab-com/issues?label_name%5B%5D=Design) label. The prioritization of projects for [about.gitlab.com](/) can be viewed on the [Website Issue Board](https://gitlab.com/gitlab-com/www-gitlab-com/boards/349137).

Any design requests that do not fall in line with the goals and objectives of Marketing will be given a lower priority and factored in as time allows.

### Design touchpoints

The Design team has a rather wide reach and plays a big part in almost all marketing efforts. Design touchpoints range from the [GitLab website](/) to print collateral, swag, and business cards. This includes, but certainly not limited to:

#### Web & Digital
{:.no_toc}
- Redesign, updates, and maintenance of the [GitLab website](/)
- Blog post covers & images
- Landing pages (campaigns, webcasts, events, and feature marketing)
- Swag shop (shop design and new swag)
- Presentation decks & assets
- Ad campaigns
- Email template design

#### Field Design & Branding
{:.no_toc}
- Marketing Design System
- Conference booth design
- Banners & signage
- Swag
- Event-specific slide decks
- Event-specific branding (e.g. GitLab World Tour, Team Summits, etc.)
- Business cards
- One-pagers, handouts, and other print collateral
- GitLab [Brand Guidelines](/handbook/marketing/corporate-marketing/design/brand-guidelines/)

#### Content Design
{:.no_toc}
- Promotional videos & animations
- Social media campaign assets
- Webcast collateral & assets
- eBooks
- Whitepapers
- Infographics & diagrams

## Brand Guidelines

To download the GitLab logo (in various formats and file types) check out our [Press page](/press/).

### GitLab Trademark & Logo Guidelines

GitLab is a registered trademark of GitLab, Inc. You are welcome to use the GitLab trademark and logo, subject to the terms of the [Creative Commons Attribution Non-Commercial ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/). The most current version of the GitLab logo can be found on our [Press page](/press/).

Under the Creative Commons license, you may use the GitLab trademark and logo so long as you give attribution to GitLab and provide a link to the license. If you make any changes to the logo, you must state so, along with the attribution, and distribute under the same license.

Your use of the GitLab trademark and logo:

- May not be for commercial purposes;
- May not suggest or imply that you or your use of the GitLab trademark or logo is endorsed by GitLab, or create confusion as to whether or not you or your use of the GitLab trademark or logo is endorsed by GitLab; and
- May not suggest or imply or that you are affiliated with GitLab in any way, or create confusion as to whether or not you are affiliated with GitLab in any way.

Examples of improper use of the GitLab trademark and logo:

- The GitLab name may not be used in any root URL, including subdomains such as `gitlab.company.com` or `gitlab.citool.io'.
- The GitLab trademark and/or logo may not be used as the primary or prominent feature on any non-GitLab materials.

### The Tanuki

The [tanuki](https://en.wikipedia.org/wiki/Japanese_raccoon_dog) is a very smart animal that works together in a group to achieve a common goal. We feel this symbolism embodies GitLab's [vision](/about/#vision) that everyone can contribute, our [values](/about/#values), and our [open source stewardship](/2016/01/11/being-a-good-open-source-steward/).

### Colors

While the brand is ever-evolving, the GitLab brand currently consists of six primary colors that are used in a wide array of marketing materials.

##### Hex/RGB

![GitLab Hex/RGB Colors](/images/handbook/marketing/corporate-marketing/design/gitlab-hex-rgb-colors.png)

### Typography

The GitLab brand is paired with two typefaces. [Lato](https://fonts.google.com/specimen/Lato) for large display type and headlines, and [Source Sans Pro](https://fonts.google.com/specimen/Source+Sans+Pro) for body copy; or main blocks of written content.

### [GitLab Product UX Guide](https://docs.gitlab.com/ee/development/ux_guide/)

The goal of this guide is to provide written standards, principles and in-depth information to design beautiful and effective GitLab features. This is a living document and will be updated and expanded as we iterate.

### [GitLab Product UX Design Pattern Library](https://brand.ai/git-lab/primary-brand/)

We've broken out the GitLab interface into a set of atomic pieces to form this design pattern library. This library allows us to see all of our patterns in one place and to build consistently across our entire product.

### Brand oversight

Occasionally the [old GitLab logo](https://gitlab.com/gitlab-com/gitlab-artwork/blob/master/archive/logo/fox.png) is still in use on partner websites, diagrams or images, and within our own documentation. If you come across our old logo in use, please bring it to our attention by creating an issue in the [Marketing](https://gitlab.com/gitlab-com/marketing/general/issues) issue tracker. Please include a link and screenshot (if possible) in the description of the issue and we will follow-up to get it updated. Thanks for contributing to our brand integrity!

## Design System

### Brand resources

- [GitLab logos](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/gitlab-logo) (web RGB & print CMYK)
- [GitLab wordmarks](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/gitlab-wordmark) (web RGB & print CMYK)
- Print-ready [event one-pagers](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/print/event-one-pagers)
- Color palette and Typography can be found in the [Brand Guidelines](/handbook/marketing/corporate-marketing/marketing-design-system/brand-guidelines/)
- [Authorized Reseller GitLab Virtuoso Badge](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/authorized-resellers/gitlab-virtuoso-badge)

### Asset libraries

#### Icons

- [Line icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/presentation-decks/_general-assets/icons/line-icons) (.png)
- [Line icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/_resources/icons/svg) (.svg)
- [Icon illustrations](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/presentation-decks/_general-assets/icons/illustrated-icons) (.png)
- [Software Development Lifecycle](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/software-development-lifecycle/complete-lifecycle-icons/png)

#### Icon patterns

- [GitLab icon pattern](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/illustrations/icon-pattern)

#### Social media

- [Profile assets](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/social-media/profile-assets/png/assets)

### Templates

#### Presentation decks

- [General GitLab deck template](https://docs.google.com/a/gitlab.com/presentation/d/16FZd01-zCj_1jApDQI7TzQMFnW2EGwtQoRuX82dkOVs/edit?usp=sharing)
- [Functional Group Update template](https://docs.google.com/a/gitlab.com/presentation/d/1JYHRhLaO9fMy1Sfr1WDnCPGv6DrlohlpOzs48VvmlQw/edit?usp=sharing)
- [GitLab pitch deck template](https://docs.google.com/a/gitlab.com/presentation/d/1LC1lT-gxpl1oUZ2InX4Oni9T4MfR0DFF0RLi4uNxBBQ/edit?usp=sharing)

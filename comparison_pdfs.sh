#!/bin/bash
filename=$1
pdffilename=$2
pdffilename=${pdffilename/.html/.pdf}

echo "👉  Generating PDF for $filename..."

wkhtmltopdf --margin-left 0 --margin-right 0 --margin-top 0 --zoom 3 $filename $pdffilename

echo "✅  PDF $pdffilename generated!"
